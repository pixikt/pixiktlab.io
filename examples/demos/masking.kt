// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/masking.js
package demos

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json("antialias" to true))
    document.body!!.appendChild(app.view)

    app.stage.interactive = true

    val bg = PIXI.Sprite.fromImage("../assets/BGrotate.jpg")

    bg.anchor.set(0.5)

    bg.x = app.screen.width.toDouble() / 2
    bg.y = app.screen.height.toDouble() / 2

    app.stage.addChild(bg)

    val container = PIXI.Container()
    container.x = app.screen.width.toDouble() / 2
    container.y = app.screen.height.toDouble() / 2

    // add a bunch of sprites
    val bgFront = PIXI.Sprite.fromImage("../assets/SceneRotate.jpg")
    bgFront.anchor.set(0.5)

    val light2 = PIXI.Sprite.fromImage("../assets/LightRotate2.png")
    light2.anchor.set(0.5)

    val light1 = PIXI.Sprite.fromImage("../assets/LightRotate1.png")
    light1.anchor.set(0.5)

    val panda = PIXI.Sprite.fromImage("../assets/panda.png")
    panda.anchor.set(0.5)

    arrayOf(bgFront, light2, light1, panda).forEach { container.addChild(it) }

    app.stage.addChild(container)

    // let's create a moving shape
    val thing = PIXI.Graphics()
    app.stage.addChild(thing)
    thing.x = app.screen.width.toDouble() / 2
    thing.y = app.screen.height.toDouble() / 2
    thing.lineStyle(0)

    container.mask = thing

    app.stage.on("pointertap", {
        if (container.mask == null) {
            container.mask = thing
        } else {
            container.mask = null
        }
    })

    val help = PIXI.Text("Click or tap to turn masking on / off.", json(
            "fontFamily" to "Arial",
            "fontSize" to 12,
            "fontWeight" to "bold",
            "fill" to "white"
    ))
    help.y = app.screen.height.toDouble() - 26
    help.x = 10
    app.stage.addChild(help)

    var count = 0.0
    app.ticker.add({
        bg.rotation = bg.rotation.toDouble() + 0.01
        bgFront.rotation = bgFront.rotation.toDouble() + 0.01

        light1.rotation = light1.rotation.toDouble() + 0.02
        light2.rotation = light2.rotation.toDouble() + 0.01

        panda.scale.x = 1 + kotlin.math.sin(count) * 0.04
        panda.scale.y = 1 + kotlin.math.cos(count) * 0.04

        count += 0.1

        thing.clear()

        thing.beginFill(0x8bc5ff, 0.4)
        thing.moveTo(-120 + kotlin.math.sin(count) * 20, -100 + kotlin.math.cos(count) * 20)
        thing.lineTo(120 + kotlin.math.cos(count) * 20, -100 + kotlin.math.sin(count) * 20)
        thing.lineTo(120 + kotlin.math.sin(count) * 20, 100 + kotlin.math.cos(count) * 20)
        thing.lineTo(-120 + kotlin.math.cos(count) * 20, 100 + kotlin.math.sin(count) * 20)
        thing.rotation = count * 0.1
    })
}
