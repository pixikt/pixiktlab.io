// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/dragging.js
@file:Suppress("DEPRECATION")

package demos

import kotlin.browser.document
import kotlin.js.Math
import kotlin.js.json

fun main(args: Array<String>) {
    data class BunnyState(
            var data: PIXI.interaction.InteractionData? = null,
            var dragging: Boolean = false
    )

    val app = PIXI.Application(800, 600, json("backgroundColor" to 0x1099bb))
    document.body!!.appendChild(app.view)

    // create a texture from an image path
    val texture = PIXI.Texture.fromImage("../assets/bunny.png")

    // Scale mode for pixelation
    texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST


    fun onDragStart(event: PIXI.interaction.InteractionEvent) {
        val bunny = event.currentTarget as PIXI.Sprite
        val bunnyState = bunny.asDynamic().bunnyState as BunnyState

        // store a reference to the data
        // the reason for this is because of multitouch
        // we want to track the movement of this particular touch
        bunnyState.data = event.data
        bunny.alpha = 0.5
        bunnyState.dragging = true
    }

    fun onDragEnd(event: PIXI.interaction.InteractionEvent) {
        val bunny = event.currentTarget as PIXI.Sprite
        val bunnyState = bunny.asDynamic().bunnyState as BunnyState

        bunny.alpha = 1
        bunnyState.dragging = false
        // set the interaction data to null
        bunnyState.data = null
    }

    fun onDragMove(event: PIXI.interaction.InteractionEvent) {
        val bunny = event.currentTarget as PIXI.Sprite
        val bunnyState = bunny.asDynamic().bunnyState as BunnyState

        if (bunnyState.dragging) {
            val newPosition = bunnyState.data!!.getLocalPosition(bunny.parent)
            bunny.x = newPosition.x
            bunny.y = newPosition.y
        }
    }

    fun createBunny(x: Double, y: Double) {
        // create our little bunny friend..
        val bunny = PIXI.Sprite(texture)
        bunny.asDynamic().bunnyState = BunnyState()

        // enable the bunny to be interactive... this will allow it to respond to mouse and touch events
        bunny.interactive = true

        // this button mode will mean the hand cursor appears when you roll over the bunny with your mouse
        bunny.buttonMode = true

        // center the bunny's anchor point
        bunny.anchor.set(0.5)

        // make it a bit bigger, so it's easier to grab
        bunny.scale.set(3)

        // setup events for mouse + touch using
        // the pointer events
        bunny
                .on("pointerdown", ::onDragStart)
                .on("pointerup", ::onDragEnd)
                .on("pointerupoutside", ::onDragEnd)
                .on("pointermove", ::onDragMove)

        // For mouse-only events
        // .on("mousedown", ::onDragStart)
        // .on("mouseup", ::onDragEnd)
        // .on("mouseupoutside", ::onDragEnd)
        // .on("mousemove", ::onDragMove);

        // For touch-only events
        // .on("touchstart", ::onDragStart)
        // .on("touchend", ::onDragEnd)
        // .on("touchendoutside", ::onDragEnd)
        // .on("touchmove", ::onDragMove);

        // move the sprite to its designated position
        bunny.x = x
        bunny.y = y

        // add it to the stage
        app.stage.addChild(bunny)
    }

    for (i in 0 until 10) {
        createBunny(
                kotlin.math.floor(Math.random() * app.screen.width.toDouble()),
                kotlin.math.floor(Math.random() * app.screen.height.toDouble())
        )
    }
}
