// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/transparent-background.js
package demos

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json("transparent" to true))
    document.body!!.appendChild(app.view)

    // create a new Sprite from an image path.
    val bunny = PIXI.Sprite.fromImage("../assets/bunny.png")

    // center the sprite's anchor point
    bunny.anchor.set(0.5)

    // move the sprite to the center of the screen
    bunny.x = app.screen.width.toDouble() / 2
    bunny.y = app.screen.height.toDouble() / 2

    app.stage.addChild(bunny)

    app.ticker.add({
        // just for fun, let's rotate mr rabbit a little
        bunny.rotation = bunny.rotation.toDouble() + 0.1
    })
}
