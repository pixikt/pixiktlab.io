// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/animatedsprite-demo.js
@file:Suppress("DEPRECATION")

package demos

import kotlin.browser.document
import kotlin.js.Math

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    app.stop()

    fun onAssetsLoaded() {

        // create an array to store the textures
        val explosionTextures = ArrayList<PIXI.Texture>()

        (1 until 28).mapTo(explosionTextures) { i -> PIXI.Texture.fromFrame("Explosion_Sequence_A $i.png") }

        for (i in 0 until 50) {
            // create an explosion AnimatedSprite
            val explosion = PIXI.extras.AnimatedSprite(explosionTextures.toTypedArray())

            explosion.x = Math.random() * app.screen.width.toDouble()
            explosion.y = Math.random() * app.screen.height.toDouble()
            explosion.anchor.set(0.5)
            explosion.rotation = Math.random() * kotlin.math.PI
            explosion.scale.set(0.75 + Math.random() * 0.5)
            explosion.gotoAndPlay(Math.random() * 27)
            app.stage.addChild(explosion)
        }

        // start animating
        app.start()
    }

    PIXI.loader
            .add("spritesheet", "../assets/mc.json")
            .load({ _, _ -> onAssetsLoaded() })
}
