// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/mask-render-texture.js
package demos

import examples.get
import kotlin.browser.document

fun main(args: Array<String>) {
    // for this example you have to use mouse or touchscreen

    val app = PIXI.Application(800, 600)
    document.body!!.appendChild(app.view)

    val stage = app.stage

    //prepare circle texture, that will be our brush
    val brush = PIXI.Graphics()
    brush.beginFill(0xffffff)
    brush.drawCircle(0, 0, 50)
    brush.endFill()

    fun setup(resources: PIXI.loaders.Resources) {
        val backgroundResource: PIXI.loaders.TextureResource = resources["t1"]
        val background = PIXI.Sprite(backgroundResource.texture)
        stage.addChild(background)
        background.width = app.screen.width
        background.height = app.screen.height

        val imageToRevealResource: PIXI.loaders.TextureResource = resources["t2"]
        val imageToReveal = PIXI.Sprite(imageToRevealResource.texture)
        stage.addChild(imageToReveal)
        imageToReveal.width = app.screen.width
        imageToReveal.height = app.screen.height

        val renderTexture = PIXI.RenderTexture.create(app.screen.width, app.screen.height)

        val renderTextureSprite = PIXI.Sprite(renderTexture)
        stage.addChild(renderTextureSprite)
        imageToReveal.mask = renderTextureSprite

        app.stage.interactive = true

        var dragging = false

        fun pointerMove(event: PIXI.interaction.InteractionEvent) {
            if (dragging) {
                brush.position.copy(event.data.global)
                app.renderer.render(brush, renderTexture, false, null, false)
            }
        }

        fun pointerDown(event: PIXI.interaction.InteractionEvent) {
            dragging = true
            pointerMove(event)
        }

        fun pointerUp() {
            dragging = false
        }

        app.stage.on("pointerdown", ::pointerDown)
        app.stage.on("pointerup", { _ -> pointerUp() })
        app.stage.on("pointermove", ::pointerMove)
    }

    PIXI.loader.add("t1", "../assets/bkg-grass.jpg")
    PIXI.loader.add("t2", "../assets/BGrotate.jpg")
    PIXI.loader.load({ _, resources -> setup(resources) })
}
