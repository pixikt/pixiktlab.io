// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/interactivity.js
package demos

import kotlin.browser.document

fun main(args: Array<String>) {
    data class ButtonState(
            var isDown: Boolean = false,
            var isOver: Boolean = false
    )

    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    // create a background...
    val background = PIXI.Sprite.fromImage("../assets/button_test_BG.jpg")
    background.width = app.screen.width
    background.height = app.screen.height

    // add background to stage...
    app.stage.addChild(background)

    // create some textures from an image path
    val textureButton = PIXI.Texture.fromImage("../assets/button.png")
    val textureButtonDown = PIXI.Texture.fromImage("../assets/buttonDown.png")
    val textureButtonOver = PIXI.Texture.fromImage("../assets/buttonOver.png")

    fun onButtonDown(event: PIXI.interaction.InteractionEvent) {
        val button = event.currentTarget as PIXI.Sprite
        button.texture = textureButtonDown
        button.alpha = 1

        val buttonState = button.asDynamic().buttonState as ButtonState
        buttonState.isDown = true
    }

    fun onButtonUp(event: PIXI.interaction.InteractionEvent) {
        val button = event.currentTarget as PIXI.Sprite
        val buttonState = button.asDynamic().buttonState as ButtonState
        buttonState.isDown = false
        if (buttonState.isOver) {
            button.texture = textureButtonOver
        } else {
            button.texture = textureButton
        }
    }

    fun onButtonOver(event: PIXI.interaction.InteractionEvent) {
        val button = event.currentTarget as PIXI.Sprite
        val buttonState = button.asDynamic().buttonState as ButtonState
        buttonState.isOver = true
        if (buttonState.isDown) {
            return
        }
        button.texture = textureButtonOver
    }

    fun onButtonOut(event: PIXI.interaction.InteractionEvent) {
        val button = event.currentTarget as PIXI.Sprite
        val buttonState = button.asDynamic().buttonState as ButtonState
        buttonState.isOver = false
        if (buttonState.isDown) {
            return
        }
        button.texture = textureButton
    }

    val buttons = ArrayList<PIXI.Sprite>()

    val buttonPositions = arrayOf(
            175, 75,
            655, 75,
            410, 325,
            150, 465,
            685, 445
    )

    for (i in 0 until 5) {
        val button = PIXI.Sprite(textureButton)
        button.asDynamic().buttonState = ButtonState()
        button.buttonMode = true

        button.anchor.set(0.5)
        button.x = buttonPositions[i * 2]
        button.y = buttonPositions[i * 2 + 1]

        // make the button interactive...
        button.interactive = true
        button.buttonMode = true

        button
                // Mouse & touch events are normalized into
                // the pointer* events for handling different
                // button events.
                .on("pointerdown", ::onButtonDown)
                .on("pointerup", ::onButtonUp)
                .on("pointerupoutside", ::onButtonUp)
                .on("pointerover", ::onButtonOver)
                .on("pointerout", ::onButtonOut)

        // Use mouse-only events
        // .on("mousedown", ::onButtonDown)
        // .on("mouseup", ::onButtonUp)
        // .on("mouseupoutside", ::onButtonUp)
        // .on("mouseover", ::onButtonOver)
        // .on("mouseout", ::onButtonOut)

        // Use touch-only events
        // .on("touchstart", ::onButtonDown)
        // .on("touchend", ::onButtonUp)
        // .on("touchendoutside", ::onButtonUp)

        // add it to the stage
        app.stage.addChild(button)

        // add button to array
        buttons.add(button)
    }

    // set some silly values...
    buttons[0].scale.set(1.2)
    buttons[2].rotation = kotlin.math.PI / 10
    buttons[3].scale.set(0.8)
    buttons[4].scale.set(0.8, 1.2)
    buttons[4].rotation = kotlin.math.PI
}
