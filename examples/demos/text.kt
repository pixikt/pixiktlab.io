// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/text-demo.js
package demos

import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLScriptElement
import org.w3c.dom.get
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    fun init() {
        fun onAssetsLoaded() {
            val bitmapFontText = PIXI.extras.BitmapText("bitmap fonts are\n now supported!", json(
                    "font" to "35px Desyrel",
                    "align" to "right"
            ))

            bitmapFontText.x = app.screen.width.toDouble() - bitmapFontText.textWidth.toDouble() - 20
            bitmapFontText.y = 20

            app.stage.addChild(bitmapFontText)
        }

        PIXI.loader
                .add("desyrel", "../assets/desyrel.xml")
                .load({ _, _ -> onAssetsLoaded() })

        // add a shiny background...
        val background = PIXI.Sprite.fromImage("../assets/textDemoBG.jpg")
        background.width = app.screen.width
        background.height = app.screen.height
        app.stage.addChild(background)

        // create some white text using the Snippet webfont
        val textSample = PIXI.Text("Pixi.js can has\n multiline text!", json(
                "fontFamily" to "Snippet",
                "fontSize" to 35,
                "fill" to "white",
                "align" to "left"
        ))
        textSample.position.set(20)

        // create a text object with a nice stroke
        val spinningText = PIXI.Text("I'm fun!", json(
                "fontWeight" to "bold",
                "fontSize" to 60,
                "fontFamily" to "Arial",
                "fill" to "#cc00ff",
                "align" to "center",
                "stroke" to "#FFFFFF",
                "strokeThickness" to 6
        ))

        // setting the anchor point to 0.5 will center align the text... great for spinning!
        spinningText.anchor.set(0.5)
        spinningText.x = app.screen.width.toDouble() / 2
        spinningText.y = app.screen.height.toDouble() / 2

        // create a text object that will be updated...
        val countingText = PIXI.Text("COUNT 4EVAR: 0", json(
                "fontWeight" to "bold",
                "fontStyle" to "italic",
                "fontSize" to 60,
                "fontFamily" to "Arvo",
                "fill" to "#3e1707",
                "align" to "center",
                "stroke" to "#a4410e",
                "strokeThickness" to 7
        ))

        countingText.x = app.screen.width.toDouble() / 2
        countingText.y = 500
        countingText.anchor.x = 0.5

        arrayOf(textSample, spinningText, countingText).forEach { app.stage.addChild(it) }

        var count = 0.0

        app.ticker.add({
            count += 0.05

            // update the text with a new string
            countingText.text = "COUNT 4EVAR: ${kotlin.math.floor(count)}"

            // let's spin the spinning text
            spinningText.rotation = spinningText.rotation.toDouble() + 0.03
        })
    }

    // Load them google fonts before starting...!
    window.asDynamic().WebFontConfig = json(
            "google" to json(
                    "families" to arrayOf("Snippet", "Arvo:700italic", "Podkova:700")
            ),
            "active" to ::init
    )

    // include the web-font loader script
    val wf = document.createElement("script") as HTMLScriptElement
    wf.src = "https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"
    wf.type = "text/javascript"
    wf.async = true
    val s = document.getElementsByTagName("script")[0] as HTMLElement
    s.parentNode!!.insertBefore(wf, s)
}
