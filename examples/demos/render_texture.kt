// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/demos/render-texture-demo.js
@file:Suppress("DEPRECATION")

package demos

import PIXI.Sprite
import kotlin.browser.document
import kotlin.js.Math

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    // create two render textures... these dynamic textures will be used to draw the scene into itself
    var renderTexture = PIXI.RenderTexture.create(
            app.screen.width,
            app.screen.height
    )

    var renderTexture2 = PIXI.RenderTexture.create(
            app.screen.width,
            app.screen.height
    )

    val currentTexture = renderTexture

    // create a new sprite that uses the render texture we created above
    val outputSprite = PIXI.Sprite(currentTexture)

    // align the sprite
    outputSprite.x = 400
    outputSprite.y = 300
    outputSprite.anchor.set(0.5)

    // add to stage
    app.stage.addChild(outputSprite)

    val stuffContainer = PIXI.Container()

    stuffContainer.x = 400
    stuffContainer.y = 300

    app.stage.addChild(stuffContainer)

    // create an array of image ids..
    val fruits = (1..8).map { i -> "../assets/spinObj_0$i.png" }

    // create some items and randomly position them in the stuff container
    val items = Array<Sprite>(20, { i ->
        val item = PIXI.Sprite.fromImage(fruits[i % fruits.size])
        item.x = Math.random() * 400 - 200
        item.y = Math.random() * 400 - 200
        item.anchor.set(0.5)
        stuffContainer.addChild(item)
        return@Array item
    })

    // used for spinning!
    var count = 0.0

    app.ticker.add({
        // rotate each item
        items.forEach { item -> item.rotation = item.rotation.toDouble() + 0.1 }

        count += 0.01

        // swap the buffers ...
        val temp = renderTexture
        renderTexture = renderTexture2
        renderTexture2 = temp

        // set the new texture
        outputSprite.texture = renderTexture

        // twist this up!
        stuffContainer.rotation = stuffContainer.rotation.toDouble() - 0.01
        outputSprite.scale.set(1 + kotlin.math.sin(count) * 0.2)

        // render the stage to the texture
        // the 'true' clears the texture before the content is rendered
        app.renderer.render(app.stage, renderTexture2, false)
    })
}
