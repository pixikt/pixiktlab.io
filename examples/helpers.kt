package examples

operator fun <T : PIXI.loaders.Resource?> PIXI.loaders.Resources.get(name: String): T =
        this.asDynamic()[name] as T