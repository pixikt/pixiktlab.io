// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/video.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "transparent" to true
    ))
    document.body!!.appendChild(app.view)

    // Create play button that can be used to trigger the video
    val button = PIXI.Graphics()
            .beginFill(0x0, 0.5)
            .drawRoundedRect(0, 0, 100, 100, 10)
            .endFill()
            .beginFill(0xffffff)
            .moveTo(36, 30)
            .lineTo(36, 70)
            .lineTo(70, 50)

    // Position the button
    button.x = (app.screen.width.toDouble() - button.width.toDouble()) / 2
    button.y = (app.screen.height.toDouble() - button.height.toDouble()) / 2

    // Enable interactivity on the button
    button.interactive = true
    button.buttonMode = true

    // Add to the stage
    app.stage.addChild(button)

    fun onPlayVideo() {
        // Don't need the button anymore
        button.destroy()

        // create a video texture from a path
        val texture = PIXI.Texture.fromVideo("../assets/testVideo.mp4")

        // create a new Sprite using the video texture (yes it's that easy)
        val videoSprite = PIXI.Sprite(texture)

        // Stetch the fullscreen
        videoSprite.width = app.screen.width
        videoSprite.height = app.screen.height

        app.stage.addChild(videoSprite)
    }

    // Listen for a click/tap event to start playing the video
    // this is useful for some mobile platforms. For example:
    // ios9 and under cannot render videos in PIXI without a
    // polyfill - https://github.com/bfred-it/iphone-inline-video
    // ios10 and above require a click/tap event to render videos
    // that contain audio in PIXI. Videos with no audio track do
    // not have this requirement
    button.on("pointertap", { onPlayVideo() })
}
