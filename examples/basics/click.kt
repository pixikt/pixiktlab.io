// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/click.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "backgroundColor" to 0x1099bb
    ))
    document.body!!.appendChild(app.view)

    // Scale mode for all textures, will retain pixelation
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST

    val sprite = PIXI.Sprite.fromImage("../assets/basics/bunny.png")

    // Set the initial position
    sprite.anchor.set(0.5)
    sprite.x = app.screen.width.toDouble() / 2
    sprite.y = app.screen.height.toDouble() / 2

    // Opt-in to interactivity
    sprite.interactive = true

    // Shows hand cursor
    sprite.buttonMode = true

    fun onClick() {
        sprite.scale.x = sprite.scale.x.toDouble() * 1.25
        sprite.scale.y = sprite.scale.y.toDouble() * 1.25
    }

    // Pointers normalize touch and mouse
    sprite.on("pointerdown", { onClick() })

    // Alternatively, use the mouse & touch events:
    // sprite.on("click", ::onClick) // mouse-only
    // sprite.on("tap", ::onClick) // touch-only

    app.stage.addChild(sprite)
}
