// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/container.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "backgroundColor" to 0x1099bb
    ))
    document.body!!.appendChild(app.view)

    val container = PIXI.Container()

    app.stage.addChild(container)

    val texture = PIXI.Texture.fromImage("../assets/basics/bunny.png")

    // Create a 5x5 grid of bunnies
    for (i in 0 until 25) {
        val bunny = PIXI.Sprite(texture)
        bunny.anchor.set(0.5)
        bunny.x = (i % 5) * 40
        bunny.y = kotlin.math.floor(i.toDouble() / 5) * 40
        container.addChild(bunny)
    }

    // Center on the screen
    container.x = (app.screen.width.toDouble() - container.width.toDouble()) / 2
    container.y = (app.screen.height.toDouble() - container.height.toDouble()) / 2
}
