// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/custom-filter.js
package basics

import kotlin.browser.document

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    // Create background image
    val background = PIXI.Sprite.fromImage("../assets/bkg-grass.jpg")
    background.width = app.screen.width
    background.height = app.screen.height
    app.stage.addChild(background)

    // Stop application wait for load to finish
    app.stop()

    var filter: PIXI.Filter? = null

    // Handle the load completed
    fun onLoaded(res: PIXI.loaders.Resources) {

        // Create the new filter, arguments: (vertexShader, framentSource)
        filter = PIXI.Filter(null, res.asDynamic().shader.data as String)

        // Add the filter
        background.filters = arrayOf(filter!!)

        // Resume application update
        app.start()
    }

    PIXI.loader.add("shader", "../assets/basics/shader.frag")
            .load({ _, res -> onLoaded(res) })

    // Animate the filter
    app.ticker.add({ delta ->
        filter!!.uniforms.customUniform += 0.04 * delta.toDouble()
    })
}
