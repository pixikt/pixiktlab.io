// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/container-pivot.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "backgroundColor" to 0x1099bb
    ))
    document.body!!.appendChild(app.view)

    val container = PIXI.Container()

    app.stage.addChild(container)

    // Create a new texture
    val texture = PIXI.Texture.fromImage("../assets/basics/bunny.png")

    // Create a 5x5 grid of bunnies
    for (i in 0 until 25) {
        val bunny = PIXI.Sprite(texture)
        bunny.anchor.set(0.5)
        bunny.x = (i % 5) * 40
        bunny.y = kotlin.math.floor(i.toDouble() / 5) * 40
        container.addChild(bunny)
    }

    // move container to the center
    container.x = app.screen.width.toDouble() / 2
    container.y = app.screen.height.toDouble() / 2

    // Center bunny sprite in local container coordinates
    container.pivot.x = container.width.toDouble() / 2
    container.pivot.y = container.height.toDouble() / 2

    // Listen for animate update
    app.ticker.add({ delta ->
        // rotate the container!
        // use delta to create frame-independent tranform
        container.rotation = container.rotation.toDouble() - 0.01 * delta.toDouble()
    })
}
