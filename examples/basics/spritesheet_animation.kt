// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/spritesheet.js
package basics

import PIXI.Texture
import kotlin.browser.document

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    fun onAssetsLoaded() {
        // create an array of textures from an image path
        val frames = ArrayList<Texture>()

        for (i in 0 until 30) {
            val value = if (i < 10) {
                "0" + i
            } else {
                i
            }

            // magically works since the spritesheet was loaded with the pixi loader
            frames.add(PIXI.Texture.fromFrame("rollSequence00$value.png"))
        }

        // create an AnimatedSprite (brings back memories from the days of Flash, right ?)
        val anim = PIXI.extras.AnimatedSprite(frames.toTypedArray())

        /*
         * An AnimatedSprite inherits all the properties of a PIXI sprite
         * so you can change its position, its anchor, mask it, etc
         */
        anim.x = app.screen.width.toDouble() / 2
        anim.y = app.screen.height.toDouble() / 2
        anim.anchor.set(0.5)
        anim.animationSpeed = 0.5
        anim.play()

        app.stage.addChild(anim)

        // Animate the rotation
        app.ticker.add({
            anim.rotation = anim.rotation.toDouble() + 0.01
        })
    }


    PIXI.loader
            .add("../assets/basics/fighter.json")
            .load({ _, _ -> onAssetsLoaded() })
}
