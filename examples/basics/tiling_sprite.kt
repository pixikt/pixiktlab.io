// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/tiling-sprite.js
package basics

import kotlin.browser.document

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    // create a texture from an image path
    val texture = PIXI.Texture.fromImage("../assets/p2.jpeg")

    /* create a tiling sprite ...
     * requires a texture, a width and a height
     * in WebGL the image size should preferably be a power of two
     */
    val tilingSprite = PIXI.extras.TilingSprite(
            texture,
            app.screen.width,
            app.screen.height
    )
    app.stage.addChild(tilingSprite)

    var count = 0.0

    app.ticker.add({
        count += 0.005

        tilingSprite.tileScale.x = 2 + kotlin.math.sin(count)
        tilingSprite.tileScale.y = 2 + kotlin.math.cos(count)

        tilingSprite.tilePosition.x = tilingSprite.tilePosition.x.toDouble() + 1
        tilingSprite.tilePosition.y = tilingSprite.tilePosition.y.toDouble() + 1
    })
}
