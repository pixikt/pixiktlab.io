// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/render-texture.js
@file:Suppress("DEPRECATION")

package basics

import kotlin.browser.document
import kotlin.js.Math
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "backgroundColor" to 0x1099bb
    ))
    document.body!!.appendChild(app.view)

    val container = PIXI.Container()
    app.stage.addChild(container)

    val texture = PIXI.Texture.fromImage("../assets/basics/bunny.png")

    for (i in 0 until 25) {
        val bunny = PIXI.Sprite(texture)
        bunny.x = (i % 5) * 30
        bunny.y = kotlin.math.floor(i.toDouble() / 5) * 30
        bunny.rotation = Math.random() * (kotlin.math.PI * 2)
        container.addChild(bunny)
    }

    val brt = PIXI.BaseRenderTexture(300, 300, PIXI.SCALE_MODES.LINEAR, 1)
    val rt = PIXI.RenderTexture(brt)

    val sprite = PIXI.Sprite(rt)

    sprite.x = 450
    sprite.y = 60
    app.stage.addChild(sprite)

    /*
     * All the bunnies are added to the container with the addChild method
     * when you do this, all the bunnies become children of the container, and when a container moves,
     * so do all its children.
     * This gives you a lot of flexibility and makes it easier to position elements on the screen
     */
    container.x = 100
    container.y = 60

    app.ticker.add({
        app.renderer.render(container, rt)
    })
}
