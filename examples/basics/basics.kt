// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/basic.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val app = PIXI.Application(800, 600, json(
            "backgroundColor" to 0x1099bb
    ))
    document.body!!.appendChild(app.view)

    // create a new Sprite from an image path
    val bunny = PIXI.Sprite.fromImage("../assets/basics/bunny.png")

    // center the sprite's anchor point
    bunny.anchor.set(0.5)

    // move the sprite to the center of the screen
    bunny.x = app.screen.width.toDouble() / 2
    bunny.y = app.screen.height.toDouble() / 2

    app.stage.addChild(bunny)

    // Listen for animate update
    app.ticker.add({ delta ->
        // just for fun, let's rotate mr rabbit a little
        // delta is 1 if running at 100% performance
        // creates frame-independent tranformation
        bunny.rotation = bunny.rotation.toDouble() + 0.1 * delta.toDouble()
    });

}
