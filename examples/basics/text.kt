// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/text.js
package basics

import kotlin.browser.document
import kotlin.js.json

fun main(args: Array<String>) {
    val applicationOptions = json(
            "backgroundColor" to 0x1099bb
    )
    val app = PIXI.Application(800, 600, applicationOptions)
    document.body!!.appendChild(app.view)

    val basicText = PIXI.Text("Basic text in pixi")
    basicText.x = 30
    basicText.y = 90

    app.stage.addChild(basicText)

    val style = PIXI.TextStyle(json(
            "fontFamily" to "Arial",
            "fontSize" to 36,
            "fontStyle" to "italic",
            "fontWeight" to "bold",
            "fill" to arrayOf("#ffffff", "#00ff99"), // gradient
            "stroke" to "#4a1850",
            "strokeThickness" to 5,
            "dropShadow" to true,
            "dropShadowColor" to "#000000",
            "dropShadowBlur" to 4,
            "dropShadowAngle" to kotlin.math.PI / 6,
            "dropShadowDistance" to 6,
            "wordWrap" to true,
            "wordWrapWidth" to 440
    ))

    val richText = PIXI.Text("Rich text with a lot of options and across multiple lines", style)
    richText.x = 30
    richText.y = 180

    app.stage.addChild(richText)
}
