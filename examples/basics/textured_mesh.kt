// ported from https://github.com/pixijs/examples/blob/gh-pages/required/examples/basics/textured-mesh.js
package basics

import kotlin.browser.document

fun main(args: Array<String>) {
    val app = PIXI.Application()
    document.body!!.appendChild(app.view)

    var count = 0.0

    // build a rope!
    val ropeLength = 45

    val points = ArrayList<PIXI.Point>()
    (0 until 25).mapTo(points) { i -> PIXI.Point(i * ropeLength, 0) }

    val strip = PIXI.mesh.Rope(PIXI.Texture.fromImage("../assets/snake.png"), points.toTypedArray())

    strip.x = -40
    strip.y = 300

    app.stage.addChild(strip)

    val g = PIXI.Graphics()
    g.x = strip.x
    g.y = strip.y
    app.stage.addChild(g)

    fun renderPoints() {
        g.clear()

        g.lineStyle(2, 0xffc2c2)
        g.moveTo(points[0].x, points[0].y)

        for (i in 1 until points.size) {
            g.lineTo(points[i].x, points[i].y)
        }

        for (i in 1 until points.size) {
            g.beginFill(0xff0022)
            g.drawCircle(points[i].x, points[i].y, 10)
            g.endFill()
        }
    }

    // start animating
    app.ticker.add({
        count += 0.1

        // make the snake
        for (i in 0 until points.size) {
            points[i].y = kotlin.math.sin((i * 0.5) + count) * 30
            points[i].x = i * ropeLength + kotlin.math.cos((i * 0.3) + count) * 20
        }
        renderPoints()
    })
}
